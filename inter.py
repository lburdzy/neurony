from __future__ import division
import Tkinter

ORIGINAL_WIDTH  = 32
ORIGINAL_HEIGHT = 32
WINDOW_WIDTH  = 512
WINDOW_HEIGHT = 512
WIDTH_RATIO  = WINDOW_WIDTH/ORIGINAL_WIDTH
HEIGHT_RATIO = WINDOW_HEIGHT/ORIGINAL_HEIGHT

class App(Tkinter.Tk):

    def __init__(self, *args, **kwargs):
        Tkinter.Tk.__init__(self, *args, **kwargs)
        self.canvas = canvas = Tkinter.Canvas( self,
                                               highlightthickness = 0,
                                               width  = WINDOW_WIDTH,
                                               height = WINDOW_HEIGHT )
        self.canvas.pack()
        # Create a list for holding canvas objects
        self.scene = []
        # Draw objects and scale them
        self.draw()
        self.zoom()


    def draw(self):
        canvas = self.canvas

        # Draw rectangles within the range of 0..32 x 0..32

        # rect1 = canvas.create_rectangle(  2,  0, 30,  2, fill='black' )
        # rect2 = canvas.create_rectangle(  0,  2,  2, 30, fill='black' )
        # rect3 = canvas.create_rectangle( 30,  2, 32, 30, fill='black' )
        # rect4 = canvas.create_rectangle(  2, 30, 30, 32, fill='black' )
        # rect5 = canvas.create_rectangle( 10, 10, 22, 22, fill='black' )
        rect4 = canvas.create_rectangle( 0, 0, 32, 32, fill='white' )
        rect5 = canvas.create_rectangle( 0, 0, 32, 32, fill='white' )
        self.canvas.bind( "<Button-1>", self.paint )
        self.scene.extend( (rect4, rect5) )

    def zoom(self):
        canvas = self.canvas
        # Scale all objects on scene
        for item in self.scene:
            canvas.scale( item, 0, 0, WIDTH_RATIO, HEIGHT_RATIO )

    def paint( self, event ):
        x1, y1 = ( event.x - 4 ), ( event.y - 4 )
        x2, y2 = ( event.x + 4 ), ( event.y + 4 )
        #self.myCanvas.create_oval( x1, y1, x2, y2, fill = "red" )
        self.canvas.create_rectangle(x1, y1, x2, y2, fill='black')

App().mainloop()